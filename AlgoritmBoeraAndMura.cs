﻿using System;
using System.Collections.Generic;
using static ConsoleLib;
using System.IO;

namespace Sortings
{
    class AlgoritmBoeraAndMura: Program
    {
        public Dictionary<char, byte> CharToByte;
        public void Start()
        {
            WriteLine("Введите слово, которое ищем: ", ConsoleColor.Yellow);
            string SubString = ConsoleLib.WriteSymb();
            CharToByte = GetWord(SubString);
            WriteLine("Введите путь до файла, в котором нужно найти нужное слово:", ConsoleColor.Yellow);
            bool IfError = true;
            while (IfError == true)
            {
                IfError = false;
                try
                {
                    SearchSubString(File.ReadAllText(ConsoleLib.WriteSymb()), SubString);
                }
                catch
                {
                    WriteLine("[ОШИБКА] Не удалось загрузить файл!", ConsoleColor.Red);
                    IfError = true;
                }
            }
            WriteSymb();
            Clear();
            Main();
        }

        public bool TryGetValue(char _char) => CharToByte.TryGetValue(_char, out byte value);

        public bool TryGetKey(char _key) => CharToByte.ContainsKey(_key);

        public Dictionary<char, byte> GetWord(string SearchedString)
        {
            CharToByte = new Dictionary<char, byte>(SearchedString.Length);
            byte count = 1;
            for (sbyte i = (sbyte)(SearchedString.Length - 2); i >= 0; i--)
            {
                if (!TryGetValue(SearchedString[i]))
                {
                    CharToByte.Add(SearchedString[i], count);
                    count++;
                }
                else
                {
                    count++;
                }
            }

            if (!TryGetValue(SearchedString[SearchedString.Length - 1]))
            {
                CharToByte.Add(SearchedString[SearchedString.Length - 1], count);
            }
            return (CharToByte);
        }

        public void SearchSubString(string _main, string _sub)
        {
            int position = -1;
            bool IfFinished = false;
            for (int i = _sub.Length - 1; i < _main.Length;)
            {
                if (IfFinished == false)
                {
                    for (int j = i; j > i - _sub.Length; j--)
                    {
                        int k = j - i + _sub.Length - 1;
                        if (!TryGetKey(_main[j]))
                        {
                            i += _sub.Length;
                            break;
                        }
                        else if ((j == i - _sub.Length + 1) && (_sub[k] == _main[j]))
                        {
                            position = j;
                            IfFinished = true;
                            break;

                        }
                        else if (_sub[k] == _main[j])
                        {
                            continue;
                        }
                        else
                        {
                            CharToByte.TryGetValue(_main[j], out byte value);
                            i += value;
                            break;
                        }
                    }
                }
                else
                {
                    break;
                }
            }

            if (position == -1)
                WriteLine("Строка не содержит такого слова!", ConsoleColor.Red);
            else
                WriteLine("Позиция первого вхождения строки: " + position.ToString() + " - " + (position + _sub.Length).ToString(), ConsoleColor.Yellow);
        }
    }
}
