﻿using System;
using static ConsoleLib;

namespace Sortings
{
    class BubbleSort: Program
    {
        int AllCounter = 0;
        int SwitchCounter = 0;

        public void Start()
        {
            int[] Numbers = GetIntegers();
            string AnswerString = "";
            for (int i = 0; i < Numbers.Length; i++)
            {
                for (int j = 0; j < Numbers.Length; j++)
                {
                    if (j > i)
                    {
                        if (Numbers[i] > Numbers[j])
                        {
                            int sub = Numbers[i];
                            Numbers[i] = Numbers[j];
                            Numbers[j] = sub;
                            i = 0;
                            SwitchCounter++;
                        }
                    }
                    AllCounter++;
                }
                AllCounter++;
            }

            for (int i = 0; i < Numbers.Length; i++)
            {
                AnswerString += Numbers[i].ToString() + " ";
            }

            WriteTwoLines("Отсортированные числа: ", AnswerString, ConsoleColor.Yellow);
            WriteTwoLines("Количество проверок: ", AllCounter.ToString(), ConsoleColor.Yellow);
            WriteTwoLines("Количество перестановок: ", SwitchCounter.ToString(), ConsoleColor.Yellow);
            EndProgram();
            Main();
        }
    }
}

