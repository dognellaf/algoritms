﻿using System;
using static ConsoleLib;

namespace Sortings
{
    class ChooseSort: Program
    {
        int AllCounter = 0;
        int SwitchCounter = 0;
        int[] Numbers;

        public void Start()
        {
            Numbers = GetIntegers();
            string AnswerString = "";
            int StartPosition = 0;
            //int EndPosition = _array.Length;


            for (int i = 0; i < Numbers.Length; i++)
            {
                int MinPos = SearchMinimum(Numbers, StartPosition);
                int Sub = Numbers[i];
                Numbers[i] = Numbers[MinPos];
                Numbers[MinPos] = Sub;
                StartPosition++;
                SwitchCounter++;
            }


            for (int i = 0; i < Numbers.Length; i++)
            {
                AnswerString += Numbers[i].ToString() + " ";
            }

            WriteTwoLines("Отсортированные числа: ", AnswerString, ConsoleColor.Yellow);
            WriteTwoLines("Количество проверок: ", AllCounter.ToString(), ConsoleColor.Yellow);
            WriteTwoLines("Количество перестановок: ", SwitchCounter.ToString(), ConsoleColor.Yellow);
            EndProgram();
            Main();
        }

        int SearchMinimum(int[] _array, int _position)
        {
            int Minimum = _array[_position];
            int MinimumPosition = _position;
            for (int i = _position; i < _array.Length; i++)
            {
                if (_array[i] < Minimum)
                {
                    Minimum = _array[i];
                    MinimumPosition = i;
                }
                AllCounter++;
            }
            return (MinimumPosition);
        }
    }
}
