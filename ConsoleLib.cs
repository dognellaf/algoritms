﻿using System;

public class ConsoleLib
{
        public static void WriteLine(string _text, ConsoleColor _color)
        {
            Console.ForegroundColor = _color;
            Console.WriteLine(_text);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void WriteLine(string _text) => Console.WriteLine(_text);

        public static void EndProgram()
        {
            WriteLine("Для продолжения нажмите ENTER", ConsoleColor.Red);
            WriteSymb();
            Clear();
        }

        public static void Write(string _text, ConsoleColor _color)
        {
            Console.ForegroundColor = _color;
            Console.Write(_text);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void Write(string _text) => Console.Write(_text);

        public static void WriteTwoLines(string FirstText, string SecondText, ConsoleColor FirstColor, ConsoleColor SecondColor)
        {
            Write(FirstText + " ", FirstColor);
            WriteLine(SecondText, SecondColor);
        }
        public static void WriteTwoLines(string FirstText, string SecondText, ConsoleColor FirstColor)
        {
            Write(FirstText + " ", FirstColor);
            WriteLine(SecondText);
        }
        public static void WriteTwoLines(string FirstText, string SecondText)
        {
            Write(FirstText + " ");
            WriteLine(SecondText);
        }

        public static string WriteSymb()
        {
            Write("> ");
            return (Console.ReadLine());
        }

        public static void Clear() => Console.Clear();
    }
