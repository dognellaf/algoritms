﻿using System;
using static ConsoleLib;

namespace Sortings
{
    class DeikAlg: Program
    {
        int Length;
        bool[] Used;
        int[,] Graf;
        int[] VertexMass;
        sbyte StartVertex;
        sbyte[] Path;


        public void Start()
        {
            WriteLine("== Алгоритм Дейкстры ==", ConsoleColor.Yellow);
            Graf = GetArray();
            Length = Graf.GetLength(0); //количество разрядностей
            VertexMass = new int[Length];
            Used = new bool[Length];
            Path = new sbyte[Length];

            WriteLine("Введите начальную вершину:", ConsoleColor.Yellow);
            while (!sbyte.TryParse(WriteSymb(), out StartVertex))
            {
                WriteLine("Ошибка! Введите число!", ConsoleColor.Red);
            }
            StartVertex--;
            //устанавливаем непромаркированным вершинам бесконечно большую величину
            for (int j = 0; j < Length; j++)
            {
                    VertexMass[j] = 100000;
            }
            VertexMass[StartVertex] = -1;

            //находим решение, начиная с четвертой вершины
            Run(StartVertex, StartVertex);

            //выводим массы
            foreach(int i in Path)
            {
                Console.Write($"{i + 1} ");
            }
        }
            //старый
            //public int Length;
            //public bool[] FlagsOfUsing;
            //public int[,] Graf;
            //public int[] Mass;
            //public byte[] Marsh;

            //public void Start(int[,] _array)
            //{
            //    //задаем базовые переменные
            //    Graf = _array;
            //    Length = _array.GetLength(0);
            //    Mass = new int[Length];
            //    Marsh = new byte[Length];
            //    FlagsOfUsing = new bool[Length];
            //    //Random rnd = new Random();

            //    //устанавливаем непромаркированным вершинам бесконечно большую величину
            //    for (int j = 0; j < Length; j++)
            //    {
            //        if (j != 3)
            //            Mass[j] = 100000;
            //        else
            //            Mass[j] = 0;
            //    }

            //    Marsh[14] = 10;
            //    Console.ReadLine();
            //    //находим решение, начиная с четвертой вершины
            //    GetAnswer(13);

            //    //вывод данных в строку
            //    WriteTwoLines("Первая вершина: ", "14", ConsoleColor.Yellow);

            //    Write("Итоговые длины маршрута: ", ConsoleColor.Yellow);
            //    Write(string.Format("{0} ", Mass[3]));
            //    int sum = 0;
            //    for (int i = 0; i < Length; i++)
            //    {
            //        if (i != 14)
            //            Write(string.Format("{0} ", Mass[i]));
            //        sum += Mass[i];
            //    }
            //    WriteLine("");

            //    WriteTwoLines("Итоговая длина маршрута:", sum.ToString(), ConsoleColor.Yellow);

            //    Write("Итоговая последовательность вершин: ", ConsoleColor.Yellow);
            //    for (int i = 0; i < Length; i++)
            //    {
            //        if (i != 14)
            //            Write(string.Format("{0} ", Marsh[i] + 1));
            //        sum += Mass[i];
            //    }
            //    WriteLine("");

            //    EndProgram();
            //    Main();
            //}

            ////deikstra.txt

            //public void GetAnswer(int Vertex)
            //{
            //    //устанавливаем метки соседям
            //    for (int j = 0; j < Length; j++)
            //    {
            //        if ((Mass[Vertex] + Graf[Vertex, j] < Mass[j])&&(Graf[Vertex, j] != 0))
            //        {
            //            Mass[j] = Mass[Vertex] + Graf[Vertex, j];
            //            Marsh[j] = (byte)Vertex;
            //        }
            //    }

            //    FlagsOfUsing[Vertex] = true;

            //    int minMass = -1;
            //    //проходим соседние графы на создание оптимального маршрута
            //    while (minMass != 1000000)
            //    {
            //        int min = -1;
            //        minMass = 1000000;

            //        for (int j = 0; j < Length; j++)
            //        {
            //            if ((Graf[Vertex, j] != 0) && !FlagsOfUsing[j] && (Mass[j] < minMass))
            //            {
            //                min = j;
            //                minMass = Mass[min];
            //            }
            //        }

            //        if (minMass != 1000000)
            //            GetAnswer(min);
            //    }

            //}

        void Run(sbyte vertex, sbyte previous)
        {
            //локальный массив для хранения посещенных вершин
            bool[] subUsed = new bool[Length];

            //ищем соседа с минимальным расстоянием
            sbyte index = SearchMin(vertex, previous, subUsed);
            //WriteLine($"Найден минимальный элемент {index + 1}");

            /*
             * Если не нашли соседа, то заканчиваем выполнение
             * Если нашли, то выполняем действия и повторяем предыдущий пункт
             */

            //цикл просмотра проставления масс
            while (index != -1)
            {
                //рассчитываем массу вершины
                int subMass = VertexMass[vertex] + Graf[index, vertex];

                //если получившаяся масса меньше старой, то устанавливаем новую
                if (subMass < VertexMass[index])
                {
                    VertexMass[index] = subMass;
                    //WriteLine($"Новая масса вершины {index + 1} = {VertexMass[index]}");
                    Path[index] = vertex;
                    //WriteLine($"Новый путь {index + 1} - {vertex + 1}");
                }
                subUsed[index] = true;

                //ищем новый минимальный элемент
                index = SearchMin(vertex, previous, subUsed);
                WriteLine($"Найден минимальный элемент {index + 1}");
            }
            //сразу отмечаем, что были в этой вершине
            Used[vertex] = true;

            index = SearchMin(vertex, previous);
            while (index != -1)
            {

                //рекурсивно ищем её соседей
                //WriteLine($"Вызвана рекурсия для {index + 1}");
                Run(index, vertex);

                //ищем новый минимальный элемент
                index = SearchMin(vertex, previous);
                //WriteLine($"Найден минимальный элемент {index + 1}");
            }
        }

        /*
         * Функция поиска минимальной вершины
         */
        sbyte SearchMin(sbyte vertex, sbyte previous, bool[] UsedIsNot = null)
        {
            //задаем первичные значения для минимального элемента и его индекса
            int min = 10000;
            sbyte index = -1;

            //проверяем каждую вершину в массиве
            for (sbyte x = 0; x < Length; x++)
            {
                /*
                 * Если расстояние между вершинами меньше минимального,
                 * Если эта вершина еще не отмечена,
                 * Если расстояние не равно нулю,
                 * Если вершина еще не была посещена на этом шаге,
                 * То считаем такую вершину минимальной
                 */
                bool flag1 = Graf[x, vertex] < min;
                bool flag2 = !Used[x];
                bool flag3 = Graf[x, vertex] != 0;
                bool flag4 = x != previous;
                bool flag5 = true;
                if (UsedIsNot != null)
                    flag5 = !UsedIsNot[x];

                //если нам нужна непосещенная вершина, указываем это
                //if (isNeedSubUsed)
                //    flag4 = !subUsed[x];
                //else
                //    flag4 = true;

                if (flag1 && flag2 && flag3 && flag4 && flag5)
                {
                    min = Graf[x, vertex];
                    index = x;
                }
            }

            return index;
        }
    }
}
