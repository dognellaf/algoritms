﻿using System;
using static ConsoleLib;

namespace Sortings
{
    class InsertSort: Program
    {

        public void Start()
        {
            int[] Numbers = GetIntegers();
            string AnswerString = "";
            int counter = 0;

            for (int i = 1; i < Numbers.Length; i++)
            {
                for (int j = i; (j > 0)&&(Numbers[j-1]> Numbers[j]); j--)
                {
                    int Sub = Numbers[j - 1];
                    Numbers[j - 1] = Numbers[j];
                    Numbers[j] = Sub;
                    counter++;
                }
            }


            for (int i = 0; i < Numbers.Length; i++)
            {
                AnswerString += Numbers[i].ToString() + " ";
            }

            WriteTwoLines("Отсортированные числа: ", AnswerString, ConsoleColor.Yellow);
            WriteTwoLines("Количество перестановок: ", counter.ToString(), ConsoleColor.Yellow);
            EndProgram();
            Main();
        }
    }
}
