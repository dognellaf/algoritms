﻿using System;
using static ConsoleLib;

namespace Sortings
{
    class KraskAlg: Program
    {
        int Length;
        bool[] FlagsOfUsing;
        int[,] Graf;

        public void Start()
        {
            Graf = GetArray();
            Length = Graf.GetLength(0);
            FlagsOfUsing = new bool[Length];

            //устанавливаем непромаркированным вершинам бесконечно большую величину
            int j = 0;
            while (HaveUnUsed())
            {
                (int x, int y) = SearchMin();
                Console.WriteLine(string.Format("[{0}] {1} - {2}", j, GetChar(x), GetChar(y)));
                j++;
            }

            Console.ReadLine();
            //находим решение, начиная с четвертой вершины

            WriteLine("");

            EndProgram();
            Main();
        }

        (int, int) SearchMin()
        {
            int min = 100000;
            (int First, int Second) Vector2 = (0, 0);
            for (int y = 0; y < Length; y++)
            {
                for (int x = 0; x < Length; x++)
                {
                    if ((Graf[x, y] < min) && (FlagsOfUsing[y] == false) && (Graf[x, y] > 0))
                    {
                        min = Graf[x, y];
                        Vector2.First = x;
                        Vector2.Second = y;
                    }
                }
            }
            FlagsOfUsing[Vector2.Second] = true;
            return Vector2;
        }

        bool HaveUnUsed()
        {
            for (int y = 0; y < Length; y++)
            {
                if (FlagsOfUsing[y] == false)
                {
                    return true;
                }
            }
            return false;
        }

        char GetChar(int i)
        {
            char ChOld;
            switch (i)
            {
                case 0: ChOld = 'a'; break;
                case 1: ChOld = 'b'; break;
                case 2: ChOld = 'c'; break;
                case 3: ChOld = 'd'; break;
                case 4: ChOld = 'e'; break;
                case 5: ChOld = 'f'; break;
                case 6: ChOld = 'g'; break;
                case 7: ChOld = 'm'; break;
                case 8: ChOld = 'k'; break;
                case 9: ChOld = 'l'; break;
                case 10: ChOld = 'n'; break;
                case 11: ChOld = 'o'; break;
                default: ChOld = '*'; break;
            }
            return ChOld;
        }
    }
}
