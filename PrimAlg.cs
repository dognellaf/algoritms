﻿using System;
using static ConsoleLib;

namespace Sortings
{
    class PrimAlg: Program
    {
        int Length;
        bool[] Used;
        int[,] Graf;
        string Answer;

        public void Start()
        {
            Graf = GetArray();
            Length = Graf.GetLength(0);
            Used = new bool[Length];
            Random rnd = new Random();
            int IndexOfI = rnd.Next(0, Length);

            //проверка графа
            Used[IndexOfI] = true;
            CheckI(IndexOfI);

            //вывод данных в строку
            WriteTwoLines("Первая случайная строка: ", IndexOfI.ToString(), ConsoleColor.Yellow);
            WriteTwoLines("Итоговая последовательность графов: ", Answer, ConsoleColor.Yellow);
            EndProgram();
            Main();
        }

        //функция проверки вершины графа
        bool CheckI(int _i)
        {
            while (FindEmptyVertexes(_i))
            {
                //ищем минимальный элемент и определяем, есть ли он вообще
                int min = 1000000;
                int minJ = 100000;
                bool HaveMove = false;
                for (int j = 0; j < Length; j++)
                {
                    if ((Used[j] == false) && (Graf[_i, j] < min) && (Graf[_i, j] != 0))
                    {
                        min = Graf[_i, j];
                        minJ = j;
                        HaveMove = true;
                    }
                }

                //если некуда ходить, переходим на предыдущий шаг
                if (HaveMove == false)
                    return false;
                else
                {
                    Used[minJ] = true;
                    AddIntToChar(_i, minJ);
                    CheckI(minJ);
                }
            }
            return true;
        }

        //функция добавления вершин в ответ
        void AddIntToChar(int _old, int _new)
        {
            char ChOld = GetChar(_old); 
            char ChNew = GetChar(_new);
            Answer += ChOld + "-" + ChNew + ", ";
        }

        //функция для получения буквы по 
        char GetChar(int i)
        {
            char ChOld;
            switch (i)
            {
                case 0: ChOld = 'a'; break;
                case 1: ChOld = 'b'; break;
                case 2: ChOld = 'c'; break;
                case 3: ChOld = 'd'; break;
                case 4: ChOld = 'e'; break;
                case 5: ChOld = 'f'; break;
                case 6: ChOld = 'g'; break;
                case 7: ChOld = 'm'; break;
                case 8: ChOld = 'k'; break;
                case 9: ChOld = 'l'; break;
                case 10: ChOld = 'n'; break;
                case 11: ChOld = 'o'; break;
                default: ChOld = '*'; break;
            }
            return ChOld;
        }

        bool FindEmptyVertexes(int _i)
        {
            for (int k = 0; k < Used.Length; k++)
            {
                if ((Used[k] == false)&&(Graf[_i, k] != 0))
                    return true;
            }
            return false;
        }

        //bool FindEmptyVertexes()
        //{
        //    for (int k = 0; k < Used.Length; k++)
        //    {
        //        if (Used[k] == false)
        //            return true;
        //    }
        //    return false;
        //}
    }
}
