﻿using System;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Runtime.Serialization.Formatters;
using System.Globalization;
using System.Reflection.Emit;
using static ConsoleLib;
using System.Collections.Generic;

namespace Sortings
{
    class Program
    {
        public static Action[] funcs;
        public static void Main()
        {
            SetFunctions();
            bool end = false;
            bool AnotherScriptRunning = false;
            while (end == false)
            {
                if (AnotherScriptRunning == false)
                {
                    WriteLine("\n_____Различные алгоритмы_____\n" +
                        "[1] Пузырьковая сортировка\n" +
                        "[2] Шейкер-сортировка\n" +
                        "[3] Алгоритм Боера и Мура\n" +
                        "[4] Сортировка вставками\n" +
                        "[5] Сортировка двоичными вставками (дерево)\n" +
                        "[6] Сортировка прямым выбором\n" +
                        "[7] Сортировка вставками с уменьшающимися расстояниями\n" +
                        "[8] Турнирная сортировка\n" +
                        "[9] Heapsort\n" +
                        "[10] Сортировка слияниями\n" +
                        "[11] MergeSort\n" +
                        "[12] Быстрая сортировка\n" +
                        "[13] Метод Шелла\n" +
                        "[14] Алгоритм Прима\n" +
                        "[15] Алгоритм Дейкстры\n" +
                        "[16] Алгоритм Краскала\n" +
                        "[17] Волновой алгоритм на массиве чисел\n" +
                        "[18] Алгоритм Кнутта-Морриса-Пратта\n" +
                        "[0] - Выход\n" +
                        "[-1] - Загрузка внешнего скрипта\n"
                        );
                    sbyte choose;
                    while (!sbyte.TryParse(ConsoleLib.WriteSymb(), out choose))
                    {
                        WriteLine("Введите число!", ConsoleColor.Red);
                    }
                    Clear();

                    if (choose == 0)
                    {
                        End();
                    }
                    else if (choose == -1)
                    {
                        Clear();
                        //WriteLine("Внимание! Пространство имен в скрипте должно быть Sortings, иначе скрипт не будет загружен!", ConsoleColor.Red);
                        WriteLine("Укажите путь до dll файла (скрипт должен находиться в папке netcoreapp2.1 и его подпапках):", ConsoleColor.Yellow);
                        try
                        {
                            Process.Start("start.bat", WriteSymb());
                            AnotherScriptRunning = true;
                        }
                        catch
                        {
                            WriteLine("Не удалось запустить файл", ConsoleColor.Red);
                            WriteSymb();
                        }
                        break;
                    }
                    else
                    {
                        if (funcs[choose] != null)
                        {
                            funcs[choose]();
                        }
                    }
                    Clear();
                }
                else if (Process.GetProcesses()[1].HasExited)
                {
                    Clear();
                    AnotherScriptRunning = false;
                }
            }
        }

        static void SetFunctions()
        {
            funcs = new Action[20];

            //пузырьковая
            BubbleSort bubble = new BubbleSort();
            funcs[1] = bubble.Start;

            //шейкер
            ShakeSorting shake = new ShakeSorting();
            funcs[2] = shake.Start;

            //алгоритм боера и мура
            AlgoritmBoeraAndMura abam = new AlgoritmBoeraAndMura();
            funcs[3] = abam.Start;

            //вставками
            InsertSort insert = new InsertSort();
            funcs[4] = insert.Start;

            //бинарным деревом
            TreeSort tree = new TreeSort();
            funcs[5] = tree.Start;

            //выборками
            ChooseSort choose = new ChooseSort();
            funcs[6] = choose.Start;

            //вставки с уменьш. значенями
            //

            //турнирная
            //

            //Heapsort
            //

            //слияниями
            //

            //MergeSort
            //

            //быстрая
            QuickSort quick = new QuickSort();
            funcs[12] = quick.Start;

            //метод шела                        
            ShellSort shell = new ShellSort();
            funcs[13] = shell.Start;

            //примм
            PrimAlg prim = new PrimAlg();
            funcs[14] = prim.Start;

            //деикстра
            DeikAlg deik = new DeikAlg();
            funcs[15] = deik.Start;

            //краскал
            KraskAlg krask = new KraskAlg();
            funcs[16] = krask.Start;

            //волновой алгоритм
            WaveAlg wave = new WaveAlg();
            funcs[17] = wave.Start;
        }

        //функция выхода из программы
        static void End() => Process.GetCurrentProcess().Kill();

        //новая функция получения чисел из файла
        public static int[] GetIntegers()
        {
            WriteLine("Введите путь до файла:", ConsoleColor.Yellow);
            string InputStr;

            ReTry:
            try
            {
                InputStr = File.ReadAllText("sortings/" + WriteSymb());
            }
            catch 
            {
                WriteLine($"Не удалось считать файл, введите правильный путь!");
                goto ReTry;
            }

            string[] StringArray = RemoveTabulation(InputStr);

            int[] Numbers = new int[StringArray.Length];

            WriteLine("");
            Write("Полученные числа: ", ConsoleColor.Yellow);

            for (int i = 0; i < StringArray.Length; i++)
            {
                Numbers[i] = int.Parse(StringArray[i]);
                Write($"{Numbers[i]} ");
            }

            WriteLine("");

            return Numbers;
        }

        //новая функция получения массива
        public static int[,] GetArray(string type = null)
        {
            WriteLine("Введите путь до файла:", ConsoleColor.Yellow);
            string InputStr;

        ReTry: //prim.txt
            try
            {
                InputStr = File.ReadAllText("algoritms/" + ConsoleLib.WriteSymb());
            }
            catch
            {
                WriteLine($"Не удалось считать файл, введите правильный путь!");
                goto ReTry;
            }

            WriteLine("Введите размерность:", ConsoleColor.Yellow);
            byte Count;
            while (!byte.TryParse(WriteSymb(), out Count))
            {
                WriteLine($"Введите правильное число!");
            }

            string[] StringArray = RemoveTabulation(InputStr);

            int[,] Numbers = new int[Count, Count];
            byte Counter = 0;

            WriteLine("");
            WriteLine("Полученные числа: ", ConsoleColor.Yellow);

            for (int y = 0; y < Count; y++)
            {
                for (int x = 0; x < Count; x++)
                {
                    Numbers[x, y] = int.Parse(StringArray[x + (Counter * Count)]);

                    if ((type == "Wave")&&(Numbers[x, y] == -1))
                        Write("■■ ", ConsoleColor.DarkGreen);
                    else if ((Numbers[x, y] > -10)&&(Numbers[x, y] < 0))
                        Write($"{Numbers[x, y]:0} ");
                    else
                        Write($"{Numbers[x, y]:00} ");
                }
                WriteLine("");
                Counter++;
            }

            WriteLine("");

            return Numbers;
        }

        //функция удаления табуляции и конвертации в массив
        static string[] RemoveTabulation(string _temp)
        {
            for (int i = 0; i < _temp.Length - 1; i++)
            {
                if (($"{_temp[i]}{_temp[i+1]}" == "\r\n"))
                    _temp = _temp.Remove(i, 2);
            }

            string[] _array = _temp.Split(new char[] { ' '});
            return _array;
        }

        /*//старая функция получения чисел
        public static int[] GetIntegers()
        {
            WriteLine("Введите путь до файла:", ConsoleColor.Yellow);
            string InputStr = "";
            try
            {
                InputStr = File.ReadAllText("sortings/" + WriteSymb());
            }
            catch { }
            int[] ArrOfIntg = new int[InputStr.Length];
            byte IndexOfIntg = 0;
            string SubStr = "";
            string AnswStr = "";

            for (int i = 0; i < InputStr.Length; i++)
            {
                if ((InputStr[i].ToString() != " "))
                    SubStr += InputStr[i];
                else
                {
                    ArrOfIntg[IndexOfIntg] = int.Parse(SubStr);
                    AnswStr += SubStr + " ";
                    SubStr = "";
                    IndexOfIntg++;
                }
                if (i == InputStr.Length - 1)
                {
                    ArrOfIntg[IndexOfIntg] = int.Parse(SubStr);
                    AnswStr += SubStr + " ";
                }
            }

            int[] Dynamic = new int[IndexOfIntg + 1];
            for (int i = 0; i < IndexOfIntg + 1; i++)
            {
                Dynamic[i] = ArrOfIntg[i];
            }

            WriteTwoLines("Полученные числа: ", AnswStr, ConsoleColor.Yellow);
            return (Dynamic);
        } */

        /*//старая функция получения массива
        public static int[,] GetArray()
        {
            //получаем строки и столбцы
            WriteLine("Введите количество строк и столбцов:", ConsoleColor.Yellow);
            int n = int.Parse(WriteSymb());
            WriteLine("Введите путь до файла:", ConsoleColor.Yellow);
            string InputStr = "";
            try
            {
                InputStr = File.ReadAllText("algoritms/" + WriteSymb());
            }
            catch { }
            int[,] ArrOfIntg = new int[n, n];
            int IndexI = 0;
            int LastSym = 0;


            string SubStr = "";
            string AnswStr = "";

            while (IndexI != n)
            {
                byte IndexOfInt = 0;
                int j;
                AnswStr += "\n";
                for (j = 0; j < n * 3; j++)
                {
                    if (LastSym + j == InputStr.Length)
                    {
                        ArrOfIntg[IndexI, IndexOfInt] = int.Parse(SubStr);
                        AnswStr += string.Format("{0:00} ", ArrOfIntg[IndexI, IndexOfInt]);
                    }
                    else if ((InputStr[j + LastSym].ToString() != " "))
                        SubStr += InputStr[j + LastSym];
                    else if (SubStr == "")
                        continue;
                    else
                    {
                        ArrOfIntg[IndexI, IndexOfInt] = int.Parse(SubStr);
                        AnswStr += string.Format("{0:00} ",ArrOfIntg[IndexI, IndexOfInt]);
                        SubStr = "";
                        IndexOfInt++;
                    }
                }
                LastSym += j;
                IndexI++;
            }

            WriteTwoLines("Полученные числа:\n", AnswStr, ConsoleColor.Yellow);
            return ArrOfIntg;
        }*/
    }
}
