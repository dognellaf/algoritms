﻿using System;
using static ConsoleLib;

namespace Sortings
{
    class QuickSort : Program
    {
        int AllCounter = 0;
        int SwitchCounter = 0;
        int[] InputArray;

        public void Start()
        {
            InputArray = GetIntegers();
            string AnswerString = "";

            QuickSorting(0, InputArray.Length - 1);
            for (int i = 0; i < InputArray.Length; i++)
            {
                AnswerString += InputArray[i].ToString() + " ";
            }

            WriteTwoLines("Отсортированные числа: ", AnswerString, ConsoleColor.Yellow);
            WriteTwoLines("Количество проверок: ", AllCounter.ToString(), ConsoleColor.Yellow);
            WriteTwoLines("Количество перестановок: ", SwitchCounter.ToString(), ConsoleColor.Yellow);
            EndProgram();
            Main();
        }

        //старая функция, которая уходила в бесконечную рекурсию

        //void SortArray(int i, int j, int LeftBorder, int RightBorder)
        //{
        //    int avg = (i + j) / 2;

        //    while (i <= j)
        //    {
        //        //проверяем левую половину
        //        while ((InputArray[i] < InputArray[avg]) && (i != avg))
        //        {
        //            i++;
        //        }

        //        //проверяем правую половину
        //        while ((InputArray[j] > InputArray[avg]) && (j != avg))
        //        {
        //            j--;
        //        }

        //        SwitchFunc(ref InputArray[i], ref InputArray[j]);
        //        i++;
        //        j--;
        //    }

        //    if (!(i == RightBorder)&&!(j == LeftBorder))
        //    {
        //        SortArray(i, RightBorder, i, RightBorder);
        //        SortArray(LeftBorder, j, LeftBorder, j);
        //    }
        //}

        void SwitchFunc(ref int first, ref int second)
        {
            int temp = second;
            AllCounter++;
            second = first;
            AllCounter++;
            first = temp;
            AllCounter++;
        }

        //новая функция
        void QuickSorting(int b, int e)
        {
            int l = b, r = e;
            int piv = InputArray[(l + r) / 2]; // Опорным элементом для примера возьмём средний
            AllCounter += 3;
            while (l <= r)
            {
                SwitchCounter++;
                while (InputArray[l] < piv)
                {
                    l++;
                    SwitchCounter++;
                    AllCounter++;
                }    
                    
                while (InputArray[r] > piv)
                {
                    r--;
                    SwitchCounter++;
                    AllCounter++;
                }
                if (l <= r)
                {
                    SwitchCounter++;
                    AllCounter++;
                    SwitchFunc(ref InputArray[l++], ref InputArray[r--]);
                }
                    
            }
            if (b < r)
            {
                SwitchCounter++;
                AllCounter++;
                QuickSorting(b, r);
            }
            if (e > l)
            {
                SwitchCounter++;
                AllCounter++;
                QuickSorting(l, e);
            }
        }   
    }
}
