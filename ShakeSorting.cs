﻿using System;
using static ConsoleLib;

namespace Sortings
{
    class ShakeSorting: Program
    {
        int AllCounter = 0;
        int SwitchCounter = 0;

        public void Start()
        {
            int[] Numbers = GetIntegers();
            string AnswerString = "";
            string Condition = "Left";
            int LeftBorder = 0, RightBorder = Numbers.Length;
            
            while (LeftBorder != RightBorder)
            {
                if (Condition == "Left")
                {
                    for (int j = LeftBorder; j < RightBorder - 1; j++)
                    {
                        if (Numbers[j + 1] > Numbers[j])
                        {
                            SwitchNumbers(ref Numbers[j], ref Numbers[j + 1]);
                        }
                        AllCounter++;
                    }
                    LeftBorder++;
                    Condition = "Right";
                }
                else if (Condition == "Right")
                {
                    for (int j = RightBorder - 1; j >= LeftBorder; j--)
                    {
                        if (Numbers[j - 1] < Numbers[j])
                        {
                            SwitchNumbers(ref Numbers[j], ref Numbers[j - 1]);
                        }
                        AllCounter++;
                    }
                    RightBorder--;
                    Condition = "Left";
                }
            }

            for (int i = Numbers.Length - 1; i >= 0; i--)
            {
                AnswerString += Numbers[i].ToString() + " ";
            }

            WriteTwoLines("Отсортированные числа: ", AnswerString, ConsoleColor.Yellow);
            WriteTwoLines("Количество проверок: ", AllCounter.ToString(), ConsoleColor.Yellow);
            WriteTwoLines("Количество перестановок: ", SwitchCounter.ToString(), ConsoleColor.Yellow);
            EndProgram();
            Main();
        }

        void SwitchNumbers(ref int one, ref int two)
        {
            int sub = one;
            one = two;
            two = sub;
            SwitchCounter++;
        }
    }
}
