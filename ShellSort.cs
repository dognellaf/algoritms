﻿using System;
using static ConsoleLib;

namespace Sortings
{
    class ShellSort : Program
    {
        int AllCounter = 0;
        int SwitchCounter = 0;
        string Answer;

        public void Start()
        {
            int[] Numbers = GetIntegers();
            int j;
            int k = Numbers.Length / 2;
            while (k > 0)
            {
                AllCounter++;
                for (int i = 0; i < (Numbers.Length - k); i++)
                {
                    j = i;
                    while ((j >= 0) && (Numbers[j] > Numbers[j + k]))
                    {
                        int tmp = Numbers[j];
                        AllCounter++;
                        Numbers[j] = Numbers[j + k];
                        AllCounter++;
                        Numbers[j + k] = tmp;
                        AllCounter++;
                        j -= k;
                        AllCounter++;
                        SwitchCounter++;
                    }
                    AllCounter++;
                }
                AllCounter++;
                k /= 2;
                AllCounter++;
            }

            for (int i = 0; i <= Numbers.Length - 1; i++)
            {
                AllCounter++;
                Answer += Numbers[i].ToString() + " ";
            }

            WriteTwoLines("Отсортированные числа: ", Answer, ConsoleColor.Yellow);
            WriteTwoLines("Количество проверок: ", AllCounter.ToString(), ConsoleColor.Yellow);
            WriteTwoLines("Количество перестановок: ", SwitchCounter.ToString(), ConsoleColor.Yellow);
            EndProgram();
            Main();
        }
    }
}
