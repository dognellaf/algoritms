﻿using System;
using static ConsoleLib;

namespace Sortings
{
    class TreeSort : Program
    {
        public int AllCounter = 0;
        public int[] Numbers;

        public void Start()
        {
            Numbers = GetIntegers();
            Branch tree = new Branch(Numbers[0]);
            for (int i = 1; i < Numbers.Length; i++)
            {
                tree.AddBranch(Numbers[i]);
                AllCounter++;
            }

            tree.GetSort();

            WriteTwoLines("Отсортированные числа: ", Branch.Answer, ConsoleColor.Yellow);
            WriteTwoLines("Количество проверок: ", (Branch.AllCounter + AllCounter).ToString(), ConsoleColor.Yellow);
            WriteTwoLines("Количество перестановок: ", Branch.SwitchCounter.ToString(), ConsoleColor.Yellow);
            EndProgram();
            Main();
        }
    }

    class Branch
    {
        public int Field;
        public static string Answer; 
        public static int AllCounter;
        public static int SwitchCounter;
        public bool IsSorted = false;
        public Branch LeftBranch;
        public Branch RightBranch;
        public Branch(int _x)
        {
            Field = _x;
            AllCounter++;
            LeftBranch = null;
            AllCounter++;
        }

        public void AddBranch(int _count)
        {
            if (_count >= Field)
                if (RightBranch != null)
                {
                    AllCounter++;
                    RightBranch.AddBranch(_count);
                }
                else
                {
                    SwitchCounter++;
                    RightBranch = new Branch(_count);
                }
            else
            {
                if (LeftBranch != null)
                {
                    AllCounter++;
                    LeftBranch.AddBranch(_count);
                }
                else
                {
                    SwitchCounter++;
                    LeftBranch = new Branch(_count);
                }
            }
        }

        public void GetSort()
        {
            if ((LeftBranch != null)&&(LeftBranch.IsSorted == false))
            {
                AllCounter++;
                LeftBranch.GetSort();
                AllCounter++;
                GetSort();
            }
                
            else
            {
                if ((RightBranch != null) && (RightBranch.IsSorted == false))
                {
                    AllCounter++;
                    SwitchCounter++;
                    Answer += Field.ToString() + " ";
                    AllCounter++;
                    IsSorted = true;
                    AllCounter++;
                    RightBranch.GetSort();
                }
                else
                {
                    AllCounter++;
                    SwitchCounter++;
                    Answer += Field.ToString() + " ";
                    AllCounter++;
                    IsSorted = true;
                }
            }
        }
    }
}
