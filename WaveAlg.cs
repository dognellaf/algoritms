﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using static ConsoleLib;

namespace Sortings
{
    class WaveAlg: Program
    {
        int[,] Labirint;
        int Length;
        short Count = 1;
        //(int x, int y) StartPosition;
        (int x, int y) EndPosition;

        public void Start()
        {
            Labirint = GetArray("Wave");
            Length = Labirint.GetLength(0);
            //StartPosition = (4, 4);
            Labirint[4, 4] = 1;
            EndPosition = (2, 19);
            while (Labirint[EndPosition.x, EndPosition.y] == 0)
            {
                for (int y = 1; y < Length - 1; y++)
                {
                    for (int x = 1; x < Length - 1; x++)
                    {
                        if (Labirint[x, y] == Count)
                        {
                            MarkNeighbors(x, y);
                        }
                    }
                }
                Count++;
            }

            string answer = "";
            for (int y = 0; y < Length; y++)
            {
                for (int x = 0; x < Length; x++)
                {
                    if (Labirint[x, y] == -1)
                        answer += $"■■■■";
                    else
                        answer += $"{Labirint[x,y]:000} ";
                }
                answer += "\n";
            }
            File.WriteAllText("./WaveAnswer.txt", answer);

            WriteLine("Результат записан в WaveAnswer.txt");
            EndProgram();
            Main();
        }

        void MarkNeighbors(int x, int y)
        {

                if ((x - 1 >= 0))
                    if (Labirint[x - 1, y] == 0)
                        Labirint[x - 1, y] = Count + 1;

                if (y - 1 >= 0)
                    if (Labirint[x, y - 1] == 0)
                        Labirint[x, y - 1] = Count + 1;

                if (x + 1 < Length)
                    if (Labirint[x + 1, y] == 0)
                        Labirint[x + 1, y] = Count + 1;

                if (y + 1 < Length)
                    if (Labirint[x, y + 1] == 0)
                        Labirint[x, y + 1] = Count + 1;
        }
    }
}
